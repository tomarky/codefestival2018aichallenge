import java.io.*;
import java.util.*;

class IO
{
    final BufferedReader in;
    final PrintStream out, err;

    IO(InputStream in, OutputStream out, OutputStream err)
    {
        this.in = new BufferedReader(new InputStreamReader(in));
        this.out = (out instanceof PrintStream)
                 ? (PrintStream)out
                 : new PrintStream(out);
        this.err = (err instanceof PrintStream)
                 ? (PrintStream)err
                 : new PrintStream(err);
    }

    IO()
    {
        this(System.in, System.out, System.err);
    }

    String readLine()
    {
        try
        {
            return in.readLine().trim();
        }
        catch (IOException ex)
        {
            throw new RuntimeException(ex);
        }
    }

    int[] readInts()
    {
        String[] tokens = readLine().split("\\s+");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < tokens.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    int readPlayTurn()
    {
        return Integer.parseInt(readLine());
    }

    Board readBoard()
    {
        Board ret = new Board();
        ret.field = new int[AI.H][];
        for (int row = 0; row < AI.H; row++)
        {
            ret.field[row] = readInts();
        }
        return ret;
    }

    Turn readTurn()
    {
        Turn ret = new Turn();
        int[] values = readInts();
        ret.turn = values[0];
        ret.timeLeft = values[1];
        ret.scoreOfTurnPlayer = values[2];
        ret.scoreOfTurnPlayer = values[3];
        ret.boardOfTurnPlayer = readBoard();
        ret.boardOfOtherPlayer = readBoard();
        return ret;
    }

    void writeFirstAttack(Pos pos)
    {
        out.printf("%d %d", pos.row + 1, pos.col + 1);
        out.println();
        out.flush();
    }

    void writeCommand(Slide slide, List<Pos> posList)
    {
        int attack = slide.getAttackValue(posList.size());
        StringBuilder sb = new StringBuilder();
        sb.append(slide.cmd);
        sb.append(' ');
        sb.append(posList.size());
        sb.append(' ');
        sb.append(attack);
        for (Pos pos : posList)
        {
            sb.append(' ');
            sb.append(pos.row + 1);
            sb.append(' ');
            sb.append(pos.col + 1);
        }
        out.println(sb.toString());
        out.flush();
    }

}

class Pos
{
    static final Pos[] POS_LIST = new Pos[AI.H * AI.W];

    static
    {
        for (int row = 0; row < AI.H; row++)
        {
            for (int col = 0; col < AI.W; col++)
            {
                POS_LIST[row * AI.W + col] = new Pos(row, col);
            }
        }
    }

    static Pos get(int row, int col)
    {
        return POS_LIST[row * AI.H + col];
    }

    final int row, col;
    Pos(int row, int col)
    {
        this.row = row;
        this.col = col;
    }
    Pos()
    {
        this(0, 0);
    }
}


class Turn
{
    int turn;
    int timeLeft;
    int scoreOfTurnPlayer;
    int scoreOfOtherPlayer;
    Board boardOfTurnPlayer;
    Board boardOfOtherPlayer;
}


class Board
{
    static final String CMD_CHARS = "URDL";
    static final int UP    = 0;
    static final int RIGHT = 1;
    static final int DOWN  = 2;
    static final int LEFT  = 3;

    int[][] field;
    int mergeCount = 0, moveCount = 0;
    List<Pos> emptyList = null;

    boolean isEmpty(int row, int col)
    {
        return field[row][col] == 0;
    }

    Board getCopy()
    {
        Board ret = new Board();
        ret.field = new int[AI.H][];
        for (int row = 0; row < AI.H; row++)
        {
            ret.field[row] = Arrays.copyOf(field[row], AI.W);
        }
        return ret;
    }

    boolean canMoveHorizontal(int startCol, int dt)
    {
        for (int row = 0; row < AI.H; row++)
        {
            int check = -1;
            int col = startCol;
            for (int count = 0; count < AI.W; count++)
            {
                if (field[row][col] > 0 && (check == 0 || check == field[row][col]))
                {
                    return true;
                }
                check = field[row][col];
                col += dt;
            }
        }
        return false;
    }

    boolean canMoveVertical(int startRow, int dt)
    {
        for (int col = 0; col < AI.W; col++)
        {
            int check = -1;
            int row = startRow;
            for (int count = 0; count < AI.H; count++)
            {
                if (field[row][col] > 0 && (check == 0 || check == field[row][col]))
                {
                    return true;
                }
                check = field[row][col];
                row += dt;
            }
        }
        return false;
    }

    boolean canMoveToLeft()
    {
        return canMoveHorizontal(0, +1);
    }
    boolean canMoveToRight()
    {
        return canMoveHorizontal(AI.W - 1, -1);
    }
    boolean canMoveToUp()
    {
        return canMoveVertical(0, +1);
    }
    boolean canMoveToDown()
    {
        return canMoveVertical(AI.H - 1, -1);
    }
    boolean canMoveTo(int f)
    {
        switch (f)
        {
        case UP:    return canMoveToUp();
        case RIGHT: return canMoveToRight();
        case DOWN:  return canMoveToDown();
        case LEFT:  return canMoveToLeft();
        }
        return false;
    }


    Board moveHorizontal(int startCol, int dt)
    {
        for (int row = 0; row < AI.H; row++)
        {
            int selCol = startCol;
            int col = startCol;
            for (int count = 0; count < AI.W; count++)
            {
                int temp = field[row][col];
                if (temp > 0)
                {
                    field[row][col] = 0;
                    if (field[row][selCol] == 0)
                    {
                        field[row][selCol] = temp;
                        if (selCol != col)
                        {
                            moveCount++;
                        }
                    }
                    else if (field[row][selCol] == temp)
                    {
                        field[row][selCol]++;
                        selCol += dt;
                        mergeCount++;
                        moveCount++;
                    }
                    else
                    {
                        selCol += dt;
                        field[row][selCol] = temp;
                        if (selCol != col)
                        {
                            moveCount++;
                        }
                    }
                }
                col += dt;
            }
        }
        return this;
    }

    Board moveVertical(int startRow, int dt)
    {
        for (int col = 0; col < AI.W; col++)
        {
            int selRow = startRow;
            int row = startRow;
            for (int count = 0; count < AI.H; count++)
            {
                int temp = field[row][col];
                if (temp > 0)
                {
                    field[row][col] = 0;
                    if (field[selRow][col] == 0)
                    {
                        field[selRow][col] = temp;
                        if (selRow != row)
                        {
                            moveCount++;
                        }
                    }
                    else if (field[selRow][col] == temp)
                    {
                        field[selRow][col]++;
                        selRow += dt;
                        mergeCount++;
                        moveCount++;
                    }
                    else
                    {
                        selRow += dt;
                        field[selRow][col] = temp;
                        if (selRow != row)
                        {
                            moveCount++;
                        }
                    }
                }
                row += dt;
            }
        }
        return this;
    }

    Board moveToLeft()
    {
        return moveHorizontal(0, +1);
    }
    Board moveToRight()
    {
        return moveHorizontal(AI.W - 1, -1);
    }
    Board moveToUp()
    {
        return moveVertical(0, +1);
    }
    Board moveToDown()
    {
        return moveVertical(AI.H - 1, -1);
    }
    Board moveTo(int f)
    {
        switch (f)
        {
        case UP:    return moveToUp();
        case RIGHT: return moveToRight();
        case DOWN:  return moveToDown();
        case LEFT:  return moveToLeft();
        }
        return this;
    }

    List<Pos> getEmptyList()
    {
        if (emptyList == null)
        {
            emptyList = new ArrayList<>();
            for (int row = 0; row < AI.H; row++)
            {
                for (int col = 0; col < AI.W; col++)
                {
                    if (field[row][col] == 0)
                    {
                        emptyList.add(Pos.get(row, col));
                    }
                }
            }
        }
        return emptyList;
    }

    int countEmpties()
    {
        if (emptyList != null)
        {
            return emptyList.size();
        }
        int ret = 0;
        for (int row = 0; row < AI.H; row++)
        {
            for (int col = 0; col < AI.W; col++)
            {
                if (field[row][col] == 0)
                {
                    ret++;
                }
            }
        }
        return ret;
    }
}

class Slide
{
    char cmd = 'U';
    int mergeCount = -1;
    int moveCount = -1;
    Slide()
    {
    }

    int getAttackValue(int posCount)
    {
        switch (posCount)
        {
        case 1:
            return mergeCount + 1; // mergeCount + 1 - 0
        case 2:
            return mergeCount;     // mergeCount + 1 - 1
        case 4:
            return mergeCount - 1; // mergeCount + 1 - 2
        case 8:
            return mergeCount - 2; // mergeCount + 1 - 3
        case 16:
            return mergeCount - 3; // mergeCount + 1 - 4
        }
        return 0;
    }

    void set(char cmd, int mergeCount, int moveCount)
    {
        this.cmd = cmd;
        this.mergeCount = mergeCount;
        this.moveCount = moveCount;
    }

    void set(int f, int mergeCount, int moveCount)
    {
        set(Board.CMD_CHARS.charAt(f & 3), mergeCount, moveCount);
    }

    void set(int f, Board board)
    {
        set(Board.CMD_CHARS.charAt(f & 3), board.mergeCount, board.moveCount);
    }

    void set(char cmd, Board board)
    {
        set(cmd, board.mergeCount, board.moveCount);
    }
}

class XorShift extends Random
{
    static final long DEFAULT_SEED = 0xCAFE_DEAD_FEED_BABEL;

    private long seed;

    XorShift()
    {
        this(DEFAULT_SEED);
    }

    XorShift(long seed)
    {
        setSeed(seed);
    }

    @Override
    public void setSeed(long seed)
    {
        if (seed == 0L)
        {
            seed = DEFAULT_SEED;
        }
        this.seed = seed;
        // super.setSeed(seed); // reset haveNextNextGaussian flag
    }

    @Override
    protected int next(int bits)
    {
        seed = seed ^ (seed << 13);
        seed = seed ^ (seed >>> 7);
        seed = seed ^ (seed << 17);
        return (int)(seed >>> (64 - bits));
    }

}

class Combination
{
    final int[] indexes;
    final int end;
    boolean finished = false;
    Combination(int end, int size)
    {
        this.end = end;
        this.indexes = new int[size];
        for (int i = 0; i < size; i++)
        {
            indexes[i] = i;
        }
        indexes[size - 1]--;
    }

    boolean next()
    {
        if (finished)
        {
            return false;
        }
        int pos = indexes.length - 1;
        for (;;)
        {
            indexes[pos]++;
            if (indexes[pos] + indexes.length - 1 - pos < end)
            {
                break;
            }
            pos--;
            if (pos < 0)
            {
                finished = true;
                return false;
            }
        }
        for (int i = pos + 1; i < indexes.length; i++)
        {
            indexes[i] = indexes[i - 1] + 1;
        }
        return true;
    }

    int get(int pos)
    {
        return indexes[pos];
    }
}

class ShuffleIndexes
{
    int[][] indexes;
    ShuffleIndexes(int maxSize)
    {
        indexes = new int[maxSize + 1][];
        for (int i = 0; i <= maxSize; i++)
        {
            int[] tmp = new int[i];
            for (int j = 0; j < i; j++)
            {
                tmp[j] = j;
            }
            indexes[i] = tmp;
        }
    }
    int[] get(int size, Random rand)
    {
        int[] tmp = indexes[size];
        for (int i = tmp.length - 1; i > 0; i--)
        {
            int j = rand.nextInt(i + 1);
            int v = tmp[i];
            tmp[i] = tmp[j];
            tmp[j] = v;
        }
        return tmp;
    }
    int[] get(int size)
    {
        return indexes[size];
    }
}

public class AI
{
    public static void main(String[] args) throws Exception
    {
        Random rand = new XorShift();
        IO io = new IO();
        AI ai = new AI(io, rand);
        ai.run();
    }

    static final int H = 5;
    static final int W = 5;

    static ShuffleIndexes shuffles = new ShuffleIndexes(H * W);

    IO io;
    Random rand;

    AI(IO io, Random rand)
    {
        this.io = io;
        this.rand = rand;
    }

    Slide quickSlide(Turn turn)
    {
        Slide ret = new Slide();

        Board[] nexts = new Board[4];
        for (int f = 0; f < 4; f++)
        {
            if (turn.boardOfTurnPlayer.canMoveTo(f))
            {
                nexts[f] = turn.boardOfTurnPlayer.getCopy().moveTo(f);
            }
        }

        for (int f = 0; f < 4; f++)
        {
            if (nexts[f] == null)
            {
                continue;
            }
            if (nexts[f].mergeCount > ret.mergeCount
                || (nexts[f].mergeCount == ret.mergeCount && nexts[f].moveCount > ret.moveCount))
            {
                ret.set(f, nexts[f]);
            }
        }

        return ret;
    }

    List<Pos> quickAttack(Turn turn, Slide slide)
    {
        List<Pos> ret = new ArrayList<>();

        List<Pos> emptyList = turn.boardOfOtherPlayer.getEmptyList();

        Pos target = emptyList.get(emptyList.size() - 1);

        ret.add(target);

        return ret;
    }

    int randomDive(int depth, Board board)
    {
        int score = 0;

        List<Pos> emptyList = board.getEmptyList();

        int[] idx = shuffles.get(emptyList.size(), rand);
        int attack = 1, attackCount = 1;
        int prob = rand.nextInt(1000);

        if (prob < 500 || emptyList.size() < 2)
        {
            attackCount = 1;
            prob = rand.nextInt(1000);
            if (prob < 300)
            {
                attack = 1;
            }
            else if (prob < 550)
            {
                attack = 2;
            }
            else if (prob < 750)
            {
                attack = 3;
            }
            else if (prob < 850)
            {
                attack = 4;
            }
            else if (prob < 950)
            {
                attack = 5;
            }
            else
            {
                attack = 6;
            }
        }
        else if (prob < 750 || emptyList.size() < 4)
        {
            attackCount = 2;
            prob = rand.nextInt(1000);
            if (prob < 400)
            {
                attack = 1;
            }
            else if (prob < 750)
            {
                attack = 2;
            }
            else
            {
                attack = 3;
            }
        }
        else if (prob < 900 || emptyList.size() < 8)
        {
            attackCount = 4;
            prob = rand.nextInt(1000);
            if (prob < 650)
            {
                attack = 1;
            }
            else if (prob < 950)
            {
                attack = 2;
            }
            else
            {
                attack = 3;
            }
        }
        else
        {
            attackCount = 8;
            attack = 1;
        }

        idx = Arrays.copyOf(idx, attackCount);

        for (int i = 0; i < attackCount; i++)
        {
            Pos pos = emptyList.get(idx[i]);
            board.field[pos.row][pos.col] = attack;
        }

        int count = 0;
        boolean[] flag = new boolean[4];

        for (int f = 0; f < 4; f++)
        {
            if (board.canMoveTo(f))
            {
                flag[f] = true;
                count++;
            }
        }

        score -= attackCount;
        score += board.mergeCount;

        if (depth > 0 && count > 0)
        {
            int sel = rand.nextInt(count);
            Board next = null;
            for (int f = 0; f < 4; f++)
            {
                if (flag[f])
                {
                    if (sel == 0)
                    {
                        next = board.getCopy().moveTo(f);
                        break;
                    }
                    sel--;
                }
            }
            int nextScore = randomDive(depth - 1, next);
            score += nextScore;
        }

        for (int i = 0; i < attackCount; i++)
        {
            Pos pos = emptyList.get(idx[i]);
            board.field[pos.row][pos.col] = 0;
        }

        return score;
    }

    Slide searchSlide(Turn turn)
    {
        Slide ret = new Slide();

        int count = 0;
        Board[] nexts = new Board[4];

        for (int f = 0; f < 4; f++)
        {
            if (turn.boardOfTurnPlayer.canMoveTo(f))
            {
                nexts[f] = turn.boardOfTurnPlayer.getCopy().moveTo(f);
                count++;
            }
        }

        if (count == 1)
        {
            for (int f = 0; f < 4; f++)
            {
                if (nexts[f] != null)
                {
                    ret.set(f, nexts[f]);
                    return ret;
                }
            }
        }

        int[] scores = new int[4];

        for (int loop = 0; loop < 600; loop++)
        {
            for (int f = 0; f < 4; f++)
            {
                if (nexts[f] == null)
                {
                    continue;
                }
                scores[f] += randomDive(9, nexts[f]);
            }
        }

        int sel = -1;

        for (int f = 0; f < 4; f++)
        {
            if (nexts[f] == null)
            {
                continue;
            }
            if (sel < 0 || scores[f] > scores[sel])
            {
                sel = f;
            }
        }

        ret.set(sel, nexts[sel]);

        return ret;
    }

    List<Pos> disturbAttack(Turn turn, Slide slide)
    {
        List<Pos> ret = new ArrayList<>();

        Board board = turn.boardOfOtherPlayer;
        List<Pos> emptyList = board.getEmptyList();
        int attackSize = slide.mergeCount + 1;

        int[] bestIndexes = null;
        int bestScore = Integer.MIN_VALUE;
        int bestNeeds = 0;
        for (int i = 1; i <= attackSize; i++)
        {
            int needs = 1 << (attackSize - i);
            if (emptyList.size() < needs)
            {
                continue;
            }
            int combSize = 1;
            for (int j = 0; j < needs; j++)
            {
                combSize *= emptyList.size() - j;
                combSize /= j + 1;
            }

            int attack = slide.getAttackValue(needs);
            if (combSize < 800)
            {
                Combination comb = new Combination(emptyList.size(), needs);
                while (comb.next())
                {
                    int[] idx = comb.indexes;
                    for (int j = 0; j < needs; j++)
                    {
                        Pos pos = emptyList.get(idx[j]);
                        board.field[pos.row][pos.col] = attack;
                    }
                    int score = 0;
                    for (int f = 0; f < 4; f++)
                    {
                        if (!board.canMoveTo(f))
                        {
                            continue;
                        }
                        Board tmp = board.getCopy().moveTo(f);
                        for (int f2 = 0; f2 < 4; f2++)
                        {
                            if (!tmp.canMoveTo(f2))
                            {
                                continue;
                            }
                            Board tmp2 = tmp.getCopy().moveTo(f2);
                            score -= (tmp.mergeCount + 1) * (tmp2.mergeCount + 1);
                        }
                    }
                    for (int j = 0; j < needs; j++)
                    {
                        Pos pos = emptyList.get(idx[j]);
                        board.field[pos.row][pos.col] = 0;
                    }
                    if (score > bestScore)
                    {
                        bestScore = score;
                        bestIndexes = Arrays.copyOf(idx, idx.length);
                        bestNeeds = needs;
                    }
                }
            }
            else
            {
                for (int loop = 8 / needs + (4 / needs) + (2 / needs) + (1 / needs) * 3 + 2; loop > 0; loop--)
                {
                    int[] idx = shuffles.get(emptyList.size(), rand);
                    for (int j = 0; j < needs; j++)
                    {
                        Pos pos = emptyList.get(idx[j]);
                        board.field[pos.row][pos.col] = attack;
                    }
                    int score = 0;
                    for (int f = 0; f < 4; f++)
                    {
                        if (!board.canMoveTo(f))
                        {
                            continue;
                        }
                        Board tmp = board.getCopy().moveTo(f);
                        for (int f2 = 0; f2 < 4; f2++)
                        {
                            if (!tmp.canMoveTo(f2))
                            {
                                continue;
                            }
                            Board tmp2 = tmp.getCopy().moveTo(f2);
                            score -= (tmp.mergeCount + 1) * (tmp2.mergeCount + 1);
                        }
                    }
                    for (int j = 0; j < needs; j++)
                    {
                        Pos pos = emptyList.get(idx[j]);
                        board.field[pos.row][pos.col] = 0;
                    }
                    if (score > bestScore)
                    {
                        bestScore = score;
                        bestIndexes = Arrays.copyOf(idx, idx.length);
                        bestNeeds = needs;
                    }
                }
            }
        }

        if (bestIndexes != null)
        {
            for (int i = 0; i < bestNeeds; i++)
            {
                ret.add(emptyList.get(bestIndexes[i]));
            }
        }

        if (ret.isEmpty())
        {
            ret.add(emptyList.get(rand.nextInt(emptyList.size())));
        }

        return ret;
    }

    Slide KillingAttack(Turn turn)
    {
        Board board = turn.boardOfOtherPlayer;
        List<Pos> emptyList = board.getEmptyList();
        if (emptyList.size() > 2)
        {
            return null;
        }
        if (emptyList.size() == 2)
        {
            Pos pos1 = emptyList.get(0);
            Pos pos2 = emptyList.get(1);
            if (Math.abs(pos1.row - pos2.row) + Math.abs(pos1.col - pos2.col) == 1)
            {
                return null;
            }
        }

        for (int f = 0; f < 4; f++)
        {
            if (!board.canMoveTo(f))
            {
                continue;
            }
            int count = board.getCopy().moveTo(f).countEmpties();
            if (count > emptyList.size())
            {
                return null;
            }
        }

        int flag = 0;
        for (Pos pos : board.getEmptyList())
        {
            if (pos.row > 0)
            {
                flag |= 1 << board.field[pos.row - 1][pos.col];
            }
            if (pos.row + 1 < H)
            {
                flag |= 1 << board.field[pos.row + 1][pos.col];
            }
            if (pos.col > 0)
            {
                flag |= 1 << board.field[pos.row][pos.col - 1];
            }
            if (pos.col + 1 < W)
            {
                flag |= 1 << board.field[pos.row][pos.col + 1];
            }
        }

        Slide slide = new Slide();
        for (int f = 0; f < 4; f++)
        {
            if (!turn.boardOfTurnPlayer.canMoveTo(f))
            {
                continue;
            }
            slide.set(f, turn.boardOfTurnPlayer.getCopy().moveTo(f));
            if (emptyList.size() > 1 && slide.mergeCount == 0)
            {
                continue;
            }
            int attack = slide.getAttackValue(emptyList.size());
            if ((flag & (1 << attack)) == 0)
            {
                io.err.println("KILL!(" + turn.turn + ")");
                return slide;
            }
        }

        return null;
    }

    void run()
    {
        int playTurn = io.readPlayTurn();

        io.writeFirstAttack(new Pos(0, 0));

        for (;;)
        {
            Turn thisTurn = io.readTurn();

            Slide slide;

            if ((slide = KillingAttack(thisTurn)) != null)
            {
                io.writeCommand(slide, thisTurn.boardOfOtherPlayer.getEmptyList());
                continue;
            }

            if (thisTurn.timeLeft < 5000)
            {
                slide = quickSlide(thisTurn);
            }
            else
            {
                slide = searchSlide(thisTurn);
            }

            List<Pos> posList;

            if (thisTurn.timeLeft < 5000)
            {
                posList = quickAttack(thisTurn, slide);
            }
            else
            {
                posList = disturbAttack(thisTurn, slide);
            }

            io.writeCommand(slide, posList);

        }
    }

}
